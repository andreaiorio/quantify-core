Welcome to Quantify's documentation!
======================================

.. include:: readme.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   installation
   usage
   tutorials/index
   contributing
   authors
   changelog


.. toctree::
   :maxdepth: 2
   :caption: API reference

   api_reference


Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
